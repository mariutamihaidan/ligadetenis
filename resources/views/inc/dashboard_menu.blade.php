<div class="sidebar-left sidebar-blog">
    <div class="widget widget-category">
        <h2 class="title-widget title18">Meniu Administrare</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/cont')}}">Dashboard </a></li>
            <li><a href="{{asset('/cont/editare')}}">Editeaza cont </a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Permisiuni</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/permisiuni/roluri')}}">Roluri</a></li>
            <li><a href="{{asset('/permisiuni/utilizatori')}}">Utilizatori</a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Provocari</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/provocari/trimise')}}">Trimise</a></li>
            <li><a href="{{asset('/provocari/primite')}}">Primite</a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Utilizatori</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/utilizatori')}}">Toti </a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Turnee</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/cont')}}">Toate turneele</a></li>
            <li><a href="{{asset('/cont')}}">Turnee in derulare</a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Articole</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/articole/editAll')}}">Toate articolele</a></li>
            <li><a href="{{asset('/categorii')}}">Categorii</a></li>
            <li><a href="{{asset('/cont')}}">Etichete</a></li>
            <li><a href="{{asset('/articole/create')}}">Adauga</a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Organizatori</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/cont')}}">Toti organizatorii</a></li>
            <li><a href="{{asset('/cont')}}">Locatii</a></li>
        </ul>
    </div>
    <div class="widget widget-category">
        <h2 class="title-widget title18">Informatii site</h2>
        <ul class="list-none list-default">
            <li><a href="{{asset('/cont')}}">Regulament</a></li>
            <li><a href="{{asset('/cont')}}">Header</a></li>
            <li><a href="{{asset('/cont')}}">Footer</a></li>
            <li><a href="{{asset('/cont')}}">Generale</a></li>
        </ul>
    </div>
</div>