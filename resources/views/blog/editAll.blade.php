@extends('layouts/frontend')
@section('header_scripts')
<link rel="stylesheet" type="text/css"    href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"></link>
@endsection
@section('footer_scripts')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    $('#datatable').DataTable();
	});
</script>
<style>
label {
    font-weight: 300;
}
table.dataTable thead th, table.dataTable tfoot th {
    font-weight: 300;
}
table.dataTable, table.dataTable th, table.dataTable td {
    font-size: 12px;
}
table.dataTable tbody td {
    padding: 5px 15px;
}
.dataTables_wrapper .dataTables_paginate .paginate_button {
    padding: 2px 10px;
}
</style>
@endsection
@section('content')


	<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Utilizatori</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('/inc/dashboard_menu')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-blog">
							<h2 class="title18 title-box5">Utilizatori</h2>
                            @if(count($articles)>0)
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Titlu</th>
                                            <th>Categoria</th>
                                            <th style="width:95px;">Actiuni</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($articles as $article)
                                            <tr>
                                                <td>{{$article->id}}</td>
                                                <td>{{$article->title}}</td>
                                                <td>Noutati</td>
                                                <td style="text-align:center;">
                                                    <a class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="right" title="Creat la {{$article->created_at}} si actualizat la {{$article->updated_at}}">
                                                        <i class="fa fa-clock-o"></i>
                                                    </a>
                                                    <a href="{{asset('/articole/'. $article->id . '/edit')}}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i></a>
                                                    <a class="btn btn-xs btn-default">
                                                        <i class="fa fa-lock"></i>
                                                    </a>
                                                    <a class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Titlu</th>
                                            <th>Categoria</th>
                                            <th style="width:95px;">Actiuni</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            @else 
                                Nu exista utilizatori in baza de date..
                            @endif
                                                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        </div>
                    </div>
				</div>
			</div>
			<!-- End Special Box -->
        </div>
    
@endsection
