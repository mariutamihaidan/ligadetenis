@extends('layouts/frontend')
@section('header_scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
@endsection
@section('footer_scripts')

@endsection
@section('content')
	<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Toate categoriile</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('/inc/dashboard_menu')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-blog">
                            <h2 class="title18 title-box5">Toate categoriile</h2>
                            @if(count($categories) > 0)
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Slug</th>
                                        <th scope="col">Titlu</th>
                                        <th scope="col">Editeaza</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)
                                        <tr>
                                            <th scope="row">{{$category->id}}</th>
                                            <td>{{$category->slug}}</td>
                                            <td>{{$category->title}}</td>
                                            <td><a class="btn btn-success btn-xs">Editeaza</a></td>
                                        </tr>
                                        @endforeach
                                        
                                      
                                    </tbody>
                                </table>
                            
                            
                            
                                
                            @else 
                                Nu exista categorii inca.. adaugati?
                            @endif
                        </div>
                    </div>
				</div>
			</div>
			<!-- End Special Box -->
        </div>
    
@endsection
