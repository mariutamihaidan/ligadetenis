@extends('layouts/frontend')
@section('content')
	<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Contul meu</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('/sidebars/locations')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-blog">
							<h2 class="title18 title-box5">{{$location->title}}</h2>
                            {{$location->description}}
                        </div>
                    </div>
				</div>
			</div>
			<!-- End Special Box -->
		</div>
@endsection