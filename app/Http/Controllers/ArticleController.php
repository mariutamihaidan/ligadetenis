<?php

namespace App\Http\Controllers;

use DB;
use App\Article;
use App\Category;
use App\ArticlesInCategories;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $articles = Article::paginate(1);
         $latestArticles = Article::orderBy('created_at', 'desc')
                         ->take(10)
                         ->get();
  
         return view('blog/index')->with(
             [
                 "latestArticles" => $latestArticles,
                 "articles" => $articles ,
                 'categories' => Category::all()
             ]
         );
    }


    public function categorie($slug)
    {
        $category = Category::where("slug",$slug)->first();
       // return $category_id;
       // return $slug;

        $articles_in_category = ArticlesInCategories::where('category_id', $category->id)->paginate(1);
        $latestArticles = Article::orderBy('created_at', 'desc')
                        ->take(10)
                        ->get();
 
        return view('blog/categorie')->with(
            [
                "latestArticles" => $latestArticles,
                "articles_in_category" => $articles_in_category ,
                'categories' => Category::all(),
                'current_category' => $category
            ]
        );
    }


    public function editAll()
    {
        $articles = DB::table('articles')->get();
        return view('blog/editAll')->with(
            [
                "articles" => $articles
            ]
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog/create')->with(
            [
                'allCategories' => Category::all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return $request;
      //  return 'dd';
        $article = new Article;
        $article->title = $request->title;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();

        if(isset($request->categories)){
            foreach($request->categories as $category){
                $article_in_category = new ArticlesInCategories;
                $article_in_category->category_id = $category;
                $article_in_category->article_id = $article->id;
                $article_in_category->save();
            }
        }

        return redirect("/articole/". $article->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $latestArticles = Article::orderBy('created_at', 'desc')->take(10)->get();
        $article = Article::find($id);
        return view('blog/show')->with(
            [
            'article' => $article,
            'latestArticles' => $latestArticles,
            'categories' => Category::all()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('blog/edit')->with(
            [
                'article' => $article,
                'allCategories' => Category::all(),
                'categories' => ArticlesInCategories::where('article_id',$article->id)->pluck('category_id')->toArray()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
       // return $request->id;
        $article = Article::find($request->id);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();

        // STergem toate categoriile din acest articol ca sa reintroducem datele noi in categorii
        $articles_in_categories = ArticlesInCategories::where('article_id',$article->id);
        $articles_in_categories->delete();
      //  return $request->categories;
        if(isset($request->categories)){
            foreach($request->categories as $category){
                $article_in_category = new ArticlesInCategories;
                $article_in_category->category_id = $category;
                $article_in_category->article_id = $request->id;
                $article_in_category->save();
            }
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
