<div class="footer">
	<div class="main-footer main-footer4">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="footer-box">
						<h2 class="title14 white"><span>Liga de tenis</span></h2>
						<ul class="list-none menu-foter">
							<li><a href="{{asset('/despre')}}" class="smoke">Despre liga</a></li>
							<li><a href="{{asset('/regulament')}}" class="smoke">Regulamentul ligii</a></li>
							<li><a href="{{asset('/clasament')}}" class="smoke">Clasamente</a></li>
							<li><a href="{{asset('/organizatori')}}" class="smoke">Organizatori</a></li>
							<li><a href="{{asset('/contact')}}" class="smoke">Contact</a></li>
							<li><a href="{{asset('/contact')}}" class="smoke">Contact</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="footer-box">
						<h2 class="title14 white"><span>Campionate</span></h2>
						<ul class="list-none menu-foter">
							<li><a href="{{asset('/jucatori')}}" class="smoke">Jucatori</a></li>
							<li><a href="{{asset('/turnee')}}" class="smoke">Turnee</a></li>
							<li><a href="{{asset('/clasament')}}" class="smoke">Clasament</a></li>
							<li><a href="{{asset('/organizatori')}}" class="smoke">Organizatori</a></li>
							<li><a href="{{asset('/blog')}}" class="smoke">Blog</a></li>
							<li><a href="{{asset('/blog')}}" class="smoke">Blog</a></li>

						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="footer-box">
						<h2 class="title14 white"><span>Articole recente</span></h2>
						<div class="list-most-post">
							@foreach(\App\Article::orderBy('id', 'desc')->take(3)->get() as $article)
								<div class="post-item table">
									<div class="post-thumb">
										<a href="{{asset('/articole/'.$article->id)}}" class="post-thumb-link">
										<img 
										src="{{ ($article->image) ? asset($article->image) : asset('images/photos/blog/b2.jpg') }}" 
										alt="" 
										/></a>
									</div>
									<div class="post-info">
										<h3><a href="{{asset('/articole/'.$article->id)}}" class="smoke">{{$article->title}}</a></h3>
										<ul class="list-inline-block color post-comment-date">
											<li><i class="fa fa-calendar-o" aria-hidden="true"></i><span>{{$article->created_at}}</span></li>
										</ul>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="footer-box">
						<h2 class="title14 white"><span>Contactati-ne!</span></h2>
						<p class="desc contact-info-footer smoke"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Piatra Neamt, Precista</p>
						<p class="desc contact-info-footer smoke"><span><i class="fa fa-phone" aria-hidden="true"></i></span>0745399958</p>
						<p class="desc contact-info-footer smoke"><span><i class="fa fa-phone" aria-hidden="true"></i></span>0765323553</p>
						<p class="desc contact-info-footer smoke"><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span><a href="maillto:info@ligadetenis.ro" class="smoke">info@ligadetenis.ro</a></p>
						<p class="desc contact-info-footer smoke"><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span><a href="maillto:info@ligadetenis.ro" class="smoke">info@ligadetenis.ro</a></p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Main Footer -->
	<div class="footer-bottom footer-bottom4">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<p class="copyright">Liga de tenis © 2019 Code Codac Web. Toate drepturile rezervate. </p>
					<p class="designby">Mentinut de <a style='color:#b18910' href="https://www.codecodac.com/">Code Codac Web</a></p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="payment-method text-right">
						<a href="#" class="wobble-top"><img src="images/icons/pay1.png" alt=""></a>
						<a href="#" class="wobble-top"><img src="images/icons/pay2.png" alt=""></a>
						<a href="#" class="wobble-top"><img src="images/icons/pay3.png" alt=""></a>
						<a href="#" class="wobble-top"><img src="images/icons/pay4.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Footer Bottom -->
</div>