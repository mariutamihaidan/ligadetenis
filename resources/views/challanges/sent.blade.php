@extends('layouts/frontend')
@section('content')
	<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Contul meu</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('/inc/dashboard_menu')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-blog">
							<h2 class="title18 title-box5">Pe cine am provocat</h2>
                            
                            @if(count($challanges) > 0)
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nume</th>
                                        <th scope="col">Locatia</th>
                                        <th scope="col">Mesaj</th>
                                        <th style="width:130px;" scope="col">Data si ora</th>
                                        <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($challanges as $challange)
                                            <tr>
                                                <th scope="row">{{$challange->id}}</th>
                                                <td>{{$challange->receiver->name}}</td>
                                                <td>
                                                    <a style="text-decoration:underline;" href="{{asset('/locatii/' . $challange->location->id)}}">
                                                        {{$challange->location->title}}
                                                    </a>
                                                </td>
                                                <td>{{$challange->message}}</td>
                                                <td>{{ date('Y-m-d H:i',$challange->timestamp) }}</td>
                                                <td>{{$challange->status}}</td>
                                            </tr>
                                        @endforeach
                                       
                                       

                                    </tbody>
                                </table>
                                @include('pagination.default', ['paginator' => $challanges])
                            @else
                                Nu ai nicio provocare trimisa!
                            @endif

                        </div>
                    </div>
				</div>
			</div>
			<!-- End Special Box -->
		</div>
@endsection