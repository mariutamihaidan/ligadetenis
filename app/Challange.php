<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challange extends Model
{
    public function location(){
        return $this->belongsTo('App\Location');
    }
    public function sender(){
        return $this->belongsTo('App\User');
    }
    public function receiver(){
        return $this->belongsTo('App\User');
    }
}
