@extends('layouts/frontend')
@section('header_scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
@endsection
@section('footer_scripts')

@endsection
@section('content')


	<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Editeaza articol</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('/inc/dashboard_menu')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-blog">
							<h2 class="title18 title-box5">Editeaza articol</h2>
                            <form action="{{asset('/articole/')}}" class="form-horizontal" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="title" class="col-xs-3 control-label">Titlu</label>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control" id="title" name="title" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="category_id" class="col-xs-3 control-label">Categorii </label>
                                    <div class="col-xs-9">
                                        @foreach($allCategories as $category)
                                            <label style="font-weight:200; margin-right:10px;" class="control-label">
                                                <input style="position:relative; top:2px;" name="categories[]" type="checkbox" value="{{$category->id}}"> {{$category->title}}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-xs-3 control-label">Descriere pentru categorie si SEO </label>
                                    <div class="col-xs-9">
                                        <textarea class="form-control required"  name="description" rows="3" maxlength="140"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="content" class="col-xs-3 control-label">Continut </label>
                                    <div class="col-xs-9">
                                        <textarea class="form-control required" name="content" id="content" rows="10"></textarea>                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-offset-3 col-xs-9">
                                        <input type="submit" class="btn btn-success" value="Actualizeaza articol">
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
				</div>
			</div>
			<!-- End Special Box -->
        </div>
    
@endsection
