<?php

namespace App\Http\Controllers;

use DB;
use App\Article;
use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::all();
        return view('categories/index')->with(
            [
                "categories" => $categories
            ]
        );







        $latestArticles = Article::orderBy('created_at', 'desc')->take(10)->get();
        $articles = DB::table('articles')->paginate(1);
        return view('blog/index')->with(
            [
                "latestArticles" => $latestArticles,
                "articles" => $articles
            ]
        );
    }

    public function editAll()
    {
        $articles = DB::table('articles')->get();
        return view('blog/editAll')->with(
            [
                "articles" => $articles
            ]
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('blog/create')->with(
            [
                'categories' => $categories
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article;
        $article->title = $request->title;
        $article->category_id = $request->category_id;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();
        return redirect("/articole/". $article->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $latestArticles = Article::orderBy('created_at', 'desc')->take(10)->get();
        $article = Article::find($id);
        return view('blog/show')->with(
            [
            'article' => $article,
            'latestArticles' => $latestArticles
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();
        return view('blog/edit')->with(
            [
                'article' => $article,
                'categories' =>  $categories
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
      //  return $request;
        $article = Article::find($request->id);
        $article->title = $request->title;
        $article->category_id = $request->category_id;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
