<?php

namespace App\Http\Controllers;

use DB;
use App\Game;
use App\User;
use App\Point;
use App\UserGames;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //   $this->recalculateTruthPoints();
       // return $this->recalculateLeague();
        return view('meciuri/index')->with(
        [
            "meciuri" => Game::paginate(10)
        ]
    );
    }


    public function recalculateLeague(){
        $this->resetPointsWithOneThousandForEachPlayer();
        $this->randomlyCreateGames();
        $this->recalculateAllLeague();
        
    }


    public function recalculateTruthPoints(){
        $this->resetPointsWithOneThousandForEachPlayer();
        $this->randomlyCreateGames();

        // $games = Game::all();
        // foreach($games as $game){
        //     $this->processUser($id);
        // }
    }

    private function recalculateAllLeague(){
        $users = User::all();
        foreach($users as $user){
            $games = UserGames::where('user_id', $user->id)->get();
            $consecutivesWon = 0;
            $consecutivesLost = 0;
            $liga = 1;
            $consecutive_need = 2; 
            foreach($games as $game){
                if($game->won == 1){
                    $consecutivesWon++;
                    if($consecutivesWon >= $consecutive_need){
                        $liga++;
                        $consecutivesWon = 0;
                    }
                    $consecutivesLost = 0;
                } else {
                    
                    $consecutivesLost++;
                    if($consecutivesLost >= $consecutive_need){
                        $liga--;
                        $consecutivesLost = 0;
                    }
                    $consecutivesWon = 0;
                }
                if($liga < 1){
                    $liga = 1;
                }
            }
          //  return $liga;
            $user = User::find($user->id);
            $user->liga = $liga;
            $user->save();
        }

    }

    private function randomlyCreateGames(){
        Game::truncate();
        UserGames::truncate();
        $players = 100;
        $games = 1000;
        for($i=0;$i<$games;$i++){
            $game = new Game;
            $game->type = rand(1,2);
            $random = [
                'p1' => rand(1,$players),
                'p2' => rand(1,$players),
                'p3' => rand(1,$players),
                'p4' => rand(1,$players),
                'winner' => rand(1,2)
            ];
            $game->p1 = $random['p1'];
            $game->p2 = $random['p2'];
            $game->p3 = $random['p3'];
            $game->p4 = $random['p4'];
            $game->result = rand(4,6) . "-" . rand(4,6) . " " . rand(4,6) . "-" . rand(4,6) ;
            $game->winner = $random['winner'];
            $game->save();

            if($game->type == 1){ // joc la simplu
                $userGames = new UserGames;
                $userGames->user_id =  $random['p1'];
                $userGames->game_id =  $game->id;
                if($game->winner == 1 AND $game->p1 == $random['p1']){
                    $userGames->won =  1;
                } else {
                    $userGames->won =  0;
                }
                $userGames->save();

                $userGames = new UserGames;
                $userGames->user_id =  $random['p3'];
                $userGames->game_id =  $game->id;
                if($game->winner == 1 AND $game->p1 == $random['p3']){
                    $userGames->won =  1;
                } else {
                    $userGames->won =  0;
                }
                $userGames->save();
            } else {
                // joc la dublu
                $userGames = new UserGames;
                $userGames->user_id =  $random['p1'];
                $userGames->game_id =  $game->id;
                if($game->winner == 1 AND ($game->p1 == $random['p1'] OR $game->p1 == $random['p2'])){
                    $userGames->won =  1;
                } else {
                    $userGames->won =  0;
                }
                $userGames->save();


                $userGames = new UserGames;
                $userGames->user_id =  $random['p2'];
                $userGames->game_id =  $game->id;
                if($game->winner == 1 AND ($game->p1 == $random['p1'] OR $game->p1 == $random['p2'])){
                    $userGames->won =  1;
                } else {
                    $userGames->won =  0;
                }
                $userGames->save();



                $userGames = new UserGames;
                $userGames->user_id =  $random['p3'];
                $userGames->game_id =  $game->id;
                if($game->winner == 1 AND ($game->p1 == $random['p3'] OR $game->p1 == $random['p4'])){
                    $userGames->won =  1;
                } else {
                    $userGames->won =  0;
                }
                $userGames->save();
                $userGames = new UserGames;
                $userGames->user_id =  $random['p4'];
                $userGames->game_id =  $game->id;
                if($game->winner == 1 AND ($game->p1 == $random['p3'] OR $game->p1 == $random['p4'])){
                    $userGames->won =  1;
                } else {
                    $userGames->won =  0;
                }
                $userGames->save();
            }

        }
    }

    private function processUser($id){
        

        if($game->type == 1){
          
        } else {

        }





        return true;
    }

    private function resetPointsWithOneThousandForEachPlayer(){
        Point::truncate();
        $users = User::all();
        foreach($users as $user){
            $points = new Point;
            $points->user_id = $user->id;
            $points->simple_game = 1000;
            $points->double_game = 1000;
            $points->save();
        }
        return true;
    }


    public function categorie($slug)
    {
        $category = Category::where("slug",$slug)->first();
       // return $category_id;
       // return $slug;

        $articles_in_category = ArticlesInCategories::where('category_id', $category->id)->paginate(1);
        $latestArticles = Article::orderBy('created_at', 'desc')
                        ->take(10)
                        ->get();
 
        return view('blog/categorie')->with(
            [
                "latestArticles" => $latestArticles,
                "articles_in_category" => $articles_in_category ,
                'categories' => Category::all(),
                'current_category' => $category
            ]
        );
    }


    public function editAll()
    {
        $articles = DB::table('articles')->get();
        return view('blog/editAll')->with(
            [
                "articles" => $articles
            ]
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog/create')->with(
            [
                'allCategories' => Category::all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return $request;
      //  return 'dd';
        $article = new Article;
        $article->title = $request->title;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();

        if(isset($request->categories)){
            foreach($request->categories as $category){
                $article_in_category = new ArticlesInCategories;
                $article_in_category->category_id = $category;
                $article_in_category->article_id = $article->id;
                $article_in_category->save();
            }
        }

        return redirect("/articole/". $article->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $latestArticles = Article::orderBy('created_at', 'desc')->take(10)->get();
        $article = Article::find($id);
        return view('blog/show')->with(
            [
            'article' => $article,
            'latestArticles' => $latestArticles,
            'categories' => Category::all()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('blog/edit')->with(
            [
                'article' => $article,
                'allCategories' => Category::all(),
                'categories' => ArticlesInCategories::where('article_id',$article->id)->pluck('category_id')->toArray()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
       // return $request->id;
        $article = Article::find($request->id);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();

        // STergem toate categoriile din acest articol ca sa reintroducem datele noi in categorii
        $articles_in_categories = ArticlesInCategories::where('article_id',$article->id);
        $articles_in_categories->delete();
      //  return $request->categories;
        if(isset($request->categories)){
            foreach($request->categories as $category){
                $article_in_category = new ArticlesInCategories;
                $article_in_category->category_id = $category;
                $article_in_category->article_id = $request->id;
                $article_in_category->save();
            }
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
