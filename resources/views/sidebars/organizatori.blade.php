<div class="sidebar-left sidebar-shop">
    <div class="widget widget-category-icon">
        <h2 class="title-widget title18">Filtre</h2>
        <div class="form-group">
            <select class="form-control">
                <option value="0">Selecteaza un judet</option>
                @foreach(\App\County::all() as $judet)
                    <option value="{{$judet->slug}}">{{$judet->name}} (24)</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="form-control">
                <option value="0">Selecteaza o localitate</option>
                @foreach(\App\City::where('county_id', 19)->get() as $localitate)
                    <option value="{{$localitate->slug}}">{{$localitate->name}} (24)</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="form-control">
                <option value="0">Selecteaza un tip</option>
                @foreach([1,2,3,4,5,6,7,8] as $liga)
                    <option value="{{$liga}}">Premium</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <select class="form-control">
                <option value="0">Selecteaza o liga</option>
                @foreach([1,2,3,4,5,6,7,8] as $liga)
                    <option value="{{$liga}}">Liga {{$liga}} (7)</option>
                @endforeach
            </select>
        </div>
    </div>
    <!-- End Widget -->
    <div class="widget widget-filter">
        <h2 class="title-widget title18">Filtre curente</h2>
        <div class="current-shop">
            <ul class="list-none">
                <li><a href="#" class="silver"> Organizatori din Neamt</a></li>
                <li><a href="#" class="silver"> Turnee active</a></li>
                <li><a href="#" class="silver"> Liga 2</a></li>
            </ul>
            <a href="#" class="clear-all black">Reseteaza filtrele</a>
        </div>
        
    </div>

</div>