<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesInCategories extends Model
{
    protected $table = "articles_in_categories";
    public function article()
    {
        return $this->belongsTo('App\Article','article_id');
    }
}
