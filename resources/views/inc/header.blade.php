		<div class="header8">
			<div class="header-main2">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-3 col-xs-5">
							<div class="logo logo2">
								<h1 class="hidden">Sports</h1>
								<a href="{{asset('/')}}"><img class="logo" src="{{asset('images/logo.png')}}" alt="" /></a>
							</div>
						</div>
						<div class="col-md-9 col-sm-9 col-xs-7">
							<div class="smart-search pull-right">
								<form class="smart-search-form">
									<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="cauta.." type="text">
									<input value="" type="submit">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Header Main -->
			<div class="header-nav8">
				<div class="container">
					<div class="nav-cart">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<nav class="main-nav main-nav2">
									<ul class="list-none">
									<li class="menu-item">
										<a href="{{asset('/')}}">HOME</a>
									</li>
									<!-- <li><a href="{{asset('/despre')}}">Despre noi</a></li> -->
									<li><a href="{{asset('/jucatori')}}">Jucatori</a></li>
									<li><a href="{{asset('/meciuri')}}">Meciuri</a></li>
									<li class="menu-item-has-children has-mega-menu">
										<a href="{{asset('/championships')}}">TURNEE</a>
										<div class="mega-menu">
											<div class="row">
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="mega-product">
														<h2 class="title18 title-box5">Most review</h2>
														<div class="list-mega-product">
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_1.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_2.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_3.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="mega-product">
														<h2 class="title18 title-box5">Special</h2>
														<div class="list-mega-product">
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_4.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_5.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_6.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3 hidden-sm hidden-xs">
													<div class="policy-sale">
														<h2 class="title18 title-box5">Save on 50%</h2>
														<p class="desc">Curabitur risus justo, scelerisque tiquensec tetur adipisicing eius mod dolore suspen disse eleme ntum lemenm</p>
														<a href="#" class="shop-button">Read more</a>
													</div>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="banner-adv fade-out-in">
														<a href="#" class="adv-thumb-link"><img src="images/menu/menu-thumb.jpg" alt="" /></a>
													</div>
												</div>
											</div>
										</div>
									</li>
									
									<li class="menu-item-has-children has-mega-menu">
										<a href="{{asset('/players')}}">CLASAMENT</a>
										<div class="mega-menu">
											<div class="row">
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="mega-product">
														<h2 class="title18 title-box5">Most review</h2>
														<div class="list-mega-product">
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_1.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_2.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_3.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="mega-product">
														<h2 class="title18 title-box5">Special</h2>
														<div class="list-mega-product">
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_4.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_5.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_6.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3 hidden-sm hidden-xs">
													<div class="policy-sale">
														<h2 class="title18 title-box5">Save on 50%</h2>
														<p class="desc">Curabitur risus justo, scelerisque tiquensec tetur adipisicing eius mod dolore suspen disse eleme ntum lemenm</p>
														<a href="#" class="shop-button">Read more</a>
													</div>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="banner-adv fade-out-in">
														<a href="#" class="adv-thumb-link"><img src="images/menu/menu-thumb.jpg" alt="" /></a>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li class="menu-item-has-children has-mega-menu">
										<a href="{{asset('/organizatori')}}">ORGANIZATORI</a>
										<div class="mega-menu">
											<div class="row">
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="mega-product">
														<h2 class="title18 title-box5">Most review</h2>
														<div class="list-mega-product">
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_1.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_2.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_3.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="mega-product">
														<h2 class="title18 title-box5">Special</h2>
														<div class="list-mega-product">
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_4.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_5.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
															<div class="item-product table">
																<div class="product-thumb">
																	<a href="detail.html" class="product-thumb-link zoom-thumb">
																		<img src="images/photos/sport_6.jpg" alt="">
																	</a>
																</div>
																<div class="product-info">
																	<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
																	<div class="product-price">
																		<del><span class="title14 silver">$798.00</span></del>
																		<ins><span class="title14 color">$399.00</span></ins>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-3 hidden-sm hidden-xs">
													<div class="policy-sale">
														<h2 class="title18 title-box5">Save on 50%</h2>
														<p class="desc">Curabitur risus justo, scelerisque tiquensec tetur adipisicing eius mod dolore suspen disse eleme ntum lemenm</p>
														<a href="#" class="shop-button">Read more</a>
													</div>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-12">
													<div class="banner-adv fade-out-in">
														<a href="#" class="adv-thumb-link"><img src="images/menu/menu-thumb.jpg" alt="" /></a>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li class="menu-item-has-children">
										<a href="{{asset('/articole')}}">Blog</a>
										<ul class="sub-menu">	
											@foreach(\App\Category::all() as $category)
												<li><a href="{{asset('/articole/categorie/'. $category->slug)}}">{{$category->title}}</a></li>
											@endforeach
										</ul>
									</li>
									
									<li><a href="{{asset('/contact')}}">Contact</a></li>
								</ul>
									<a href="#" class="toggle-mobile-menu"><span></span></a>
								</nav>
								<!-- End Main nav -->
								<ul class="list-inline-block cart-account pull-right">
								@if (Auth::check())
									<li><a href="{{asset('/cont')}}" class="title14 white"><img src="{{asset('/images/icons/icon-user.png')}}" alt="" />Contul meu</a></li>
									<li>
										<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										<img src="{{asset('/images/icons/icon-logout.png')}}" alt="" style="filter: invert(100%);  width:19px;"/>
										</a>
									</li>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								@else
									<li><a href="{{asset('/cont')}}" class="title14 white"><img src="{{asset('/images/icons/icon-user.png')}}" alt="" />Intra in cont</a></li>
								@endif
									<li><a href="{{asset('/regulament')}}" class="title14 white"><img src="{{asset('/images/icons/contract.png')}}" alt="" style="filter: invert(100%);  width:19px;"/>Regulament</a></li>
									<li>
										<div class="mini-cart-box">
											<a class="mini-cart-link title12 white" href="cart.html">
												<span class="mini-cart-icon inline-block"><img src="{{asset('/images/icons/notification.png')}}" alt="" style="filter: invert(100%); width:19px;"/><sup class="bg-color white round">0</sup></span>
											</a>
											<div class="mini-cart-content text-left">
												<h2 class="title18">(2) Alerte..</h2>
												<div class="list-mini-cart-item">
													
													<div class="product-mini-cart table">
													Felicitari! Ai primit 6 luni gratuite!
													</div>
													<div class="product-mini-cart table">
													Felicitari! Ai primit 6 luni gratuite!
													</div>
													<div class="product-mini-cart table">
													Felicitari! Ai primit 6 luni gratuite!
													</div>
													<div class="product-mini-cart table">
													Felicitari! Ai primit 6 luni gratuite!
													</div>
												</div>
												<div class="mini-cart-button">
													<a class="mini-cart-view shop-button" href="#">Toate</a>
													<a class="mini-cart-checkout shop-button" href="#">Sterge</a>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>