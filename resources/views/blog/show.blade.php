@extends('layouts/frontend')
@section('content')
<div class="container">
			<div class="bread-crumb">
				<a href="{{asset('/')}}" class="silver">Home</a>
				<a href="{{asset('/articole')}}" class="silver">Blog</a>
				<span class="color">{{$article->title}}</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<div class="sidebar-left sidebar-blog">
							<div class="widget widget-recent-post">
								<h2 class="title-widget title18">Articole recente</h2>
								@if(count($latestArticles) > 0)
									<ul class="list-none list-default">
										@foreach($latestArticles as $latestArticle)
											<li><a href="{{asset('/articole/'. $latestArticle->id)}}">{{$latestArticle->title}}</a></li>
										@endforeach
									</ul>
								@else 
									Nu exista articole
								@endif
								
							</div>
							<!-- End Widget -->
							<div class="widget widget-category">
								<h2 class="title-widget title18">Categories</h2>
								<ul class="list-none list-default">
									@foreach($categories as $category)
										<li><a href="{{asset('/articole/categorie/'. $category->slug)}}">{{$category->title}}</a></li>
                                    @endforeach
								</ul>
							</div>
							<!-- End Widget -->
						</div>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-single">
							<h2 class="title18 title-box5">{{$article->title}}</h2>
							<div class="post-title-date">
								<div class="postdate">
									<strong class="title30">11</strong>
									<span class="title12">Sep 2017</span>
								</div>
								<div class="post-title-comment">
									<h2 class="title14">{{$article->title}}</h2>
									<ul class="list-inline-block">
										<li><span>Posted By</span> <a href="#" class="silver">7uptheme</a></li>
										<li><span>5</span><a href="#comment" class="silver"> Comments</a></li>
									</ul>
								</div>
							</div>
							<div class="single-banner banner-adv zoom-image fade-out-in">
								<a href="#" class="adv-thumb-link"><img src="images/blog/single.jpg" alt=""></a>
							</div>
							<p class="desc">{!!$article->content!!}</p>
							<div class="post-social-share">
								<p>Distribuiti acest articol</p>
								<ul class="list-inline-block">
									<li><a href="#" class="silver"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#" class="silver"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									<li><a href="#" class="silver"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#" class="silver"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#" class="silver"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							<div class="comment-list">
								<h2 class="title18">3 Comment</h2>
								<ol>
									<li>
										<div class="item-comment">
											<div class="author-avatar"><a href="#"><img src="images/blog/av2.jpg" alt=""></a></div>
											<div class="comment-info">
												<h3 class="title14"><a href="#">Janine Desmines</a></h3>
												<ul class="post-date-comment">
													<li><span>8 hours ago</span></li>
													<li><a href="#comment"><i class="fa fa-reply-all" aria-hidden="true"></i>Reply</a></li>
												</ul>
												<p class="desc">Patience is the most underestimated virtue of them all (of course, disqualifying honesty, decency, and ethics from the list of options). Id has explicari conceptam delicatissimi. Alii enim mediocrem ei per. Per at vocent persius, ut qui hinc nostro cetero</p>
											</div>
										</div>
										<ul>
											<li>
												<div class="item-comment">
													<div class="author-avatar"><a href="#"><img src="images/blog/av3.jpg" alt=""></a></div>
													<div class="comment-info">
														<h3 class="title14"><a href="#">Fanbong Pham</a></h3>
														<ul class="post-date-comment">
															<li><span>9 hours ago</span></li>
															<li><a href="#comment"><i class="fa fa-reply-all" aria-hidden="true"></i>Reply</a></li>
														</ul>
														<p class="desc">Patience is the most underestimated virtue of them all (of course, disqualifying honesty, decency, and ethics from the list of options). Id has explicari conceptam delicatissimi. Alii enim mediocrem ei per. Per at vocent persius, ut qui hinc nostro cetero</p>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li>
										<div class="item-comment">
											<div class="author-avatar">
												<a href="#"><img src="images/blog/av1.jpg" alt=""></a>
											</div>
											<div class="comment-info">
												<h3 class="title14"><a href="#">Janine Desmines</a></h3>
												<ul class="post-date-comment">
													<li><span>18 hours ago</span></li>
													<li><a href="#comment"><i class="fa fa-reply-all" aria-hidden="true"></i>Reply</a></li>
												</ul>
												<p class="desc"> Id has explicari conceptam delicatissimi. Alii enim mediocrem ei per. Per at vocent persius, ut qui hinc nostro cetero. Patience is the most underestimated virtue of them all (of course, disqualifying honesty, decency, and ethics from the list of options).</p>
											</div>
										</div> 
									</li> 
								</ol>
							</div> 
							<div class="leave-comment">
								<h2 class="title18">Leave a Reply</h2>
								<p>Your email address will not be published. Required fields are marked <span>*</span></p>
								<form class="comment-form">
									<input class="border radius4" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Full Name*" name="name" type="text">
									<input class="border radius4" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Email Address*" name="email" type="text">
									<input class="border radius4" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Url Site" name="web" type="text">
									<textarea class="border radius4" name="message" cols="30" rows="6" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''">Comment*</textarea>
									<input class="radius4 shop-button" value="Post Comment" type="submit">
								</form>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="list-service">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form1.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Order Online</a></h3>
									<h4 class="title14 transition">Hours: 8AM -11PM</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block item-active active">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form2.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Save 30% </a></h3>
									<h4 class="title14 transition">When you use credit card</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form3.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Free Shipping</a></h3>
									<h4 class="title14 transition">On orders over $99</h4>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End List Service -->
			<div class="list-special-box">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Specials</h2>
							<div class="product-special">
								<div class="special-slider">
									<div class="wrap-item navi-bottom" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1]]">
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_1.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_2.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_3.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
									</div>
								</div>
								<a href="#" class="shop-button">View all specials</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Newsletter</h2>
							<div class="newletter-form">
								<p class="desc">Make sure you dont miss interesting hap penings by joining our newsletter program.</p>
								<form class="email-form">
									<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="your e-mail address" type="text">
									<input class="shop-button" value="Subscribe" type="submit">Locația ta
								</form>
							</div>
							<h2 class="title18 font-bold">Connect with us</h2>
							<div class="social-network">
								<a href="#" class="float-shadow"><img src="images/icons/icon-fb.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-tw.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-li.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-gp.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-pt.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-sk.png" alt="" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 hidden-sm col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Video sport</h2>
							<div class="box-video">
								<a href="#" class="video-lightbox"><img src="images/home/home1/video-img.png" alt="" /></a>
								<h3 class="title14"><a href="#">Lorem ipsum dolor sit amet</a></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Special Box -->
		</div>
@endsection