<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGames extends Model
{
    protected $table = 'user_games';

    public function details(){
        return $this->belongsTo('App\Game','game_id');
    }
}
