<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function player1(){
        return $this->hasOne('App\User','id','p1');
    }

    public function player2(){
        return $this->hasOne('App\User','id','p2');
    }

    public function player3(){
        return $this->hasOne('App\User','id','p3');
    }

    public function player4(){
        return $this->hasOne('App\User','id','p4');
    }
}
