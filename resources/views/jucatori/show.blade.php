@extends('layouts/frontend')
@section('content')
<section id="content">
	<div class="container">
		<div class="bread-crumb">
			<a href="#" class="silver">Home</a><a href="{{asset('/jucatori')}}" class="silver">Jucatori </a><span class="color">{{$jucator->name}}</span>
		</div>
		<div class="content-detail">
			<div class="product-detail">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="detail-gallery">
							<div class="item-product">
								<span class="product-label sale-label">Liga<br />{{$jucator->liga}}</span>
								@if(!$jucator->image)
									<img style="width:100%;" src="{{asset('/storage/images/genders/'.$jucator->gender.'.png')}}" alt="">
								@else 
									<img style="width:100%;" src="{{asset('/storage/images/players/' . $jucator->id .'/'.$jucator->image)}}" alt="">
								@endif     
								<hr />
								                        
							</div>
						</div>
						<!-- End Gallery -->
					</div>
					<div class="col-md-7 col-sm-6 col-xs-12">
						<div class="detail-info">
							<h2 class="title-detail title30">{{$jucator->name}}</h2>
							<ul class="list-inline-block sku-stock">
								<li>Piatra Neamt</li>
								<li>Neamt</li>
								<li>Club Tare</li>
								<li>Liga {{$jucator->liga}}</li>
								<li><i class="fa fa-angle-up" aria-hidden="true"></i> {{$jucator->points->simple_game}}</li>
								<li><i class="fa fa-angle-double-up" aria-hidden="true"></i>  {{$jucator->points->double_game}}</li>
							</ul>
							<p class="desc">{{$jucator->autodescription}}</p>

							<div class="detail-descript">
								<h2 class="title18"><span>Ultimele meciuri</span></h2>
								<p class="desc">
									Dan Mihai Mariuta - <span class="winner_player">Costea Octavian</span> ( 6-4 4-6 8-10 ) <br />
									Dan Mihai Mariuta - <span class="winner_player">Costea Octavian</span> ( 6-4 4-6 8-10 ) <br />
									Dan Mihai Mariuta - <span class="winner_player">Costea Octavian</span> ( 6-4 4-6 8-10 ) <br />
									Dan Mihai Mariuta - <span class="winner_player">Costea Octavian</span> ( 6-4 4-6 8-10 ) <br />	
								</p>
							</div>
							
							@if(App\Http\Controllers\ChallangesController::challangeExists(\Auth::user()->id, $jucator->id))		
								<p class="title14 shop-button challange_duel">Acest jucator a fost provocat la un meci!</p>  
								<a href="#" class="title14 shop-button">Lasa un mesaj privat</a>
								<p>
								<br />
								Duelul va avea loc in Piatra Neamt, vineri 11 decembrie 2019 ora 19:30
								</p>
							@else 
							<a href="#"  data-toggle="modal" data-target="#duel" class="title14 shop-button challange_duel">Provoaca la duel</a>  
							<a href="#" class="title14 shop-button">Lasa un mesaj privat</a>
							@endif
							
						</div>			
					</div>
				</div>
			</div>
			<div class="detail-tabs">
				<div class="title-tab-detail">
					<ul class="list-inline-block">
						<li class="active"><a href="#tab1" class="title14" data-toggle="tab">Portofoliu</a></li>
						<li><a href="#tab2" class="title14" data-toggle="tab">Meciuri simplu</a></li>
						<li><a href="#tab3" class="title14" data-toggle="tab">Meciuri dublu</a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div id="tab1" class="tab-pane active">
						<div class="detail-descript">
						
							<p class="desc">
								Pozitia in clasament la simplu: 102 <br />
								Pozitia in clasament la dublu: 893 <br />
							</p>
						</div>
					</div>
					<div id="tab2" class="tab-pane">
						<div class="detail-addition">
							<table class="table table-bordered table-striped">
								<tbody>
									<tr>
										<td><p class="desc">Frame Material: Wood</p></td>
										<td><p class="desc">Seat Material: Wood</p></td>
									</tr>
									<tr>
										<td><p class="desc">Adjustable Height: No</p></td>
										<td><p class="desc">Seat Style: Saddle</p></td>
									</tr>
									<tr>
										<td><p class="desc">Distressed: No</p></td>
										<td><p class="desc">Custom Made: No</p></td>
									</tr>
									<tr>
										<td><p class="desc">Number of Items Included: 1</p></td>
										<td><p class="desc">Folding: No</p></td>
									</tr>
									<tr>
										<td><p class="desc">Stackable: No</p></td>
										<td><p class="desc">Cushions Included: No</p></td>
									</tr>
									<tr>
										<td><p class="desc">Arms Included: No</p></td>
										<td>
											<div class="product-more-info">
												<p class="desc">Legs Included: Yes</p>
												<ul class="list-none">
													<li><a href="#">Leg Material: Wood</a></li>
													<li><a href="#">Number of Legs: 4</a></li>
												</ul>
											</div>
										</td>
									</tr>
									<tr>
										<td><p class="desc">Footrest Included: Yes</p>	</td>
										<td><p class="desc">Casters Included: No</p></td>
									</tr>
									<tr>
										<td><p class="desc">Nailhead Trim: No</p></td>
										<td><p class="desc">Weight Capacity: 225 Kilogramm</p></td>
									</tr>
									<tr>
										<td><p class="desc">Commercial Use: No</p></td>
										<td><p class="desc">Country of Manufacture: Vietnam</p></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="tab3" class="tab-pane">
						<div class="content-tags-detail">
							<h3 class="title14">2 Review for bakery macaron</h3>
							<ul class="list-none list-tags-review">
								<li>
									<div class="review-author">
										<a href="#"><img src="images/shop/author1.jpg" alt=""></a>
									</div>
									<div class="review-info">
										<p class="review-header"><a href="#"><strong>7up-theme</strong></a> &ndash; March 30, 2017:</p>
										<div class="product-rate">
											<div class="product-rating" style="width:100%"></div>
										</div>
										<p class="desc">Really a nice stool. It was better than I expected in quality. The color is a rich, honey brown and looks a little lighter than pictured but still a great stool for the money.</p>
									</div>
								</li>
								<li>
									<div class="review-author">
										<a href="#"><img src="images/shop/author2.jpg" alt=""></a>
									</div>
									<div class="review-info">
										<p class="review-header"><a href="#"><strong>7up-theme</strong></a> &ndash; March 30, 2017:</p>
										<div class="product-rate">
											<div class="product-rating" style="width:100%"></div>
										</div>
										<p class="desc">Really a nice stool. It was better than I expected in quality. The color is a rich, honey brown and looks a little lighter than pictured but still a great stool for the money.</p>
									</div>
								</li>
							</ul>
							<div class="add-review-form">
								<h3 class="title14">Add a Review</h3>
								<p>Your email address will not be published. Required fields are marked *</p>
								<form class="review-form">
									<div>
										<label>Name *</label>
										<input name="name" id="name" type="text">
									</div>
									<div>
										<label>Email *</label>
										<input name="email" id="email" type="text">
									</div>
									<div>
										<label>Your Rating</label>
										<div class="product-rate">
											<div class="product-rating" style="width:100%"></div>
										</div>
									</div>
									<div>
										<label>Your Review *</label>
										<textarea name="messasge" id="message" cols="30" rows="10"></textarea>
									</div>
									<div>
										<input class="shop-button radius4" value="Submit" type="submit">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="list-service">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<ul class="item-service list-inline-block">
						<li>
							<div class="service-icon">
								<a href="#"><img class="wobble-horizontal" src="images/home/home1/form1.png" alt="" /></a>
							</div>
						</li>
						<li>
							<div class="service-info">
								<h3 class="title18 font-bold"><a href="#" class="black">Order Online</a></h3>
								<h4 class="title14 transition">Hours: 8AM -11PM</h4>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<ul class="item-service list-inline-block item-active active">
						<li>
							<div class="service-icon">
								<a href="#"><img class="wobble-horizontal" src="images/home/home1/form2.png" alt="" /></a>
							</div>
						</li>
						<li>
							<div class="service-info">
								<h3 class="title18 font-bold"><a href="#" class="black">Save 30% </a></h3>
								<h4 class="title14 transition">When you use credit card</h4>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<ul class="item-service list-inline-block">
						<li>
							<div class="service-icon">
								<a href="#"><img class="wobble-horizontal" src="images/home/home1/form3.png" alt="" /></a>
							</div>
						</li>
						<li>
							<div class="service-info">
								<h3 class="title18 font-bold"><a href="#" class="black">Free Shipping</a></h3>
								<h4 class="title14 transition">On orders over $99</h4>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End List Service -->
		<div class="list-special-box">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="special-box border">
						<h2 class="title18 font-bold">Specials</h2>
						<div class="product-special">
							<div class="special-slider">
								<div class="wrap-item navi-bottom" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1]]">
									<div class="item-product">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb"><img src="images/photos/sport_1.jpg" alt="" /></a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
											<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
										</div>
									</div>
									<div class="item-product">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb"><img src="images/photos/sport_2.jpg" alt="" /></a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
											<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
										</div>
									</div>
									<div class="item-product">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb"><img src="images/photos/sport_3.jpg" alt="" /></a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
											<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
										</div>
									</div>
								</div>
							</div>
							<a href="#" class="shop-button">View all specials</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="special-box border">
						<h2 class="title18 font-bold">Newsletter</h2>
						<div class="newletter-form">
							<p class="desc">Make sure you dont miss interesting hap penings by joining our newsletter program.</p>
							<form class="email-form">
								<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="your e-mail address" type="text">
								<input class="shop-button" value="Subscribe" type="submit">
							</form>
						</div>
						<h2 class="title18 font-bold">Connect with us</h2>
						<div class="social-network">
							<a href="#" class="float-shadow"><img src="images/icons/icon-fb.png" alt="" /></a>
							<a href="#" class="float-shadow"><img src="images/icons/icon-tw.png" alt="" /></a>
							<a href="#" class="float-shadow"><img src="images/icons/icon-li.png" alt="" /></a>
							<a href="#" class="float-shadow"><img src="images/icons/icon-gp.png" alt="" /></a>
							<a href="#" class="float-shadow"><img src="images/icons/icon-pt.png" alt="" /></a>
							<a href="#" class="float-shadow"><img src="images/icons/icon-sk.png" alt="" /></a>
						</div>
					</div>
				</div>
				<div class="col-md-4 hidden-sm col-xs-12">
					<div class="special-box border">
						<h2 class="title18 font-bold">Video sport</h2>
						<div class="box-video">
							<a href="#" class="video-lightbox"><img src="images/home/home1/video-img.png" alt="" /></a>
							<h3 class="title14"><a href="#">Lorem ipsum dolor sit amet</a></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Special Box -->
	</div>
</section>

<div class="modal fade" id="duel" tabindex="-1" role="dialog" aria-labelledby="contactlLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
	   <form class="block-login" method="POST" action="{{ asset('/provocari') }}">
			@csrf
			
			<div class="leave-comment">
				<form class="comment-form">
					<textarea maxlength="510" style="width:100%;" class="border radius4" name="message" rows="6" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''">Alte detalii de clarificare a duelului *</textarea>
					<div class="form-group">
						<label for="exampleFormControlSelect1">Locatia</label>
						<select class="form-control" id="exampleFormControlSelect1" name="location_id">
							@foreach( \App\Location::get() as $location)
							<option value="{{$location->id}}">{{$location->title}}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group">
						Data si ora propusa pentru duel
						<input class="form-control" name="timestamp" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
					</div>
				<p style="color:red;">
					Ar trebui sa pun aici 'selecteaza judet'>' -> '<'selecteaza locatie'> pentru locatii
					Poti primi sau pierde 12 puncte in clasament (5% din punctajul tau curent), iar {{$jucator->name}} poate primi sau pierde 28 puncte (5% din punctajul sau curent) <br />
					Campurile marcate cu <span>*</span> sunt obligatorii<br />
					Vom trimite organizatorului si persoanei pe care o provocati numarul de telefon, pentru a stabili detaliile duelului!
				</p>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">M-am razgandit</button>
			<button type="submit" class="btn btn-sm btn-danger">Trimite provocarea</button>
		</div>
		<input type="hidden" name="receiver_id" value="{{$jucator->id}}" />
	  </form>
    </div>
  </div>
</div>
@endsection