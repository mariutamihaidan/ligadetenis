<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Sport is new Html theme that we have designed to help you transform your store into a beautiful online showroom. This is a fully responsive Html theme, with multiple versions for homepage and multiple templates for sub pages as well" />
	<meta name="keywords" content="Sport,7uptheme" />
	<meta name="robots" content="noodp,index,follow" />
	<meta name='revisit-after' content='1 days' />
	 <!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Liga de tenis</title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/font-awesome.min.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/bootstrap.min.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/bootstrap-theme.min.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/jquery.fancybox.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/jquery-ui.min.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/owl.carousel.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/owl.transitions.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/owl.theme.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/slick.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/flipclock.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/animate.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/libs/hover.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/color.css')}}" media="all"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/theme.css')}}" media="all"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/responsive.css')}}" media="all"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/css/browser.css')}}" media="all"/>
	<!-- <link rel="stylesheet" type="text/css" href="css/rtl.css" media="all"/> -->
	
	@yield('header_scripts')
</head>

<body>
<div class="wrap">
	<header id="header">
	@include('inc/header')
	</header>
	<!-- End Header -->
	<section id="content">
		@yield('content')
	</section>
	<!-- End Content -->
	<footer id="footer">
	@include('inc/footer')
	</footer>
	<!-- End Footer -->
	<div class="wishlist-mask">
		<div class="wishlist-popup">
			<span class="popup-icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
			<p class="wishlist-alert">"Sport Product" was added to wishlist</p>
			<div class="wishlist-button">
				<a href="#">Continue Shopping (<span class="wishlist-countdown">10</span>)</a>
				<a href="#">Go To Shopping Cart</a>
			</div>
		</div>
	</div>
	<!-- End Wishlist Mask -->
	<a href="#" class="scroll-top rect"><i class="fa fa-angle-up" aria-hidden="true"></i><span>TOP</span></a>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="{{asset('/js/libs/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/libs/jquery.fancybox.js')}}"></script>
<script src="{{asset('/js/libs/jquery-ui.min.js')}}"></script>
<script src="{{asset('/js/libs/owl.carousel.min.js')}}"></script>
<script src="{{asset('/js/libs/jquery.jcarousellite.min.js')}}"></script>
<script src="{{asset('/js/libs/jquery.elevatezoom.js')}}"></script>
<script src="{{asset('/js/libs/jquery.mCustomScrollbar.js')}}"></script>
<script src="{{asset('/js/libs/jquery.bxslider.js')}}"></script>
<script src="{{asset('/js/libs/slick.js')}}"></script>
<script src="{{asset('/js/libs/popup.js')}}"></script>
<script src="{{asset('/js/libs/flipclock.js')}}"></script>
<script src="{{asset('/js/libs/wow.js')}}"></script>
<script src="{{asset('/js/theme.js')}}"></script>
@yield('footer_scripts')
</body>
</html>