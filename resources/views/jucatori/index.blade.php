@extends('layouts/frontend')
@section('content')
<section id="content">
		<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Jucatori</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<div class="sidebar-left sidebar-shop">
                          
							<!-- End Widget -->
							<div class="widget widget-filter">
                            <h2 class="title-widget title18">Filtrare jucatori</h2>
								<div class="current-shop">
                                    <h3 class="title14">Dupa nume</h3>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ex: Popescu">
                                    </div>
								</div>
								
								
								<div class="current-shop">
                                    <h3 class="title14">Meciuri jucate</h3>
									<div class="range-filter">
										<span class="amount"></span>
										<button class="btn-filter-price shop-button title14">Filtreaza</button>
										<div class="slider-range"></div>
									</div>
                                </div>
                                
                                <div class="current-shop">
                                    <h3 class="title14" style="margin-bottom: 15px;">Liga</h3>
                                    <div class="pagi-bar">
                                        @for($i=1;$i<7; $i++)
                                            <a data-lige="{{$i}}" onclick="filterLeague(this)" class="league_filter">{{$i}}</a>
                                        @endfor
                                    </div>   
                                </div>	

								<div class="current-shop">
									<h3 class="title14">Judet</h3>
                                    <div class="form-group">
                                        <select  class="form-control" id="county_id" onchange="countyChanged(this);">
											@foreach(\App\County::orderBy('name', 'asc')->get() as $judet)
												<option 
												@if(isset($county_id))
													@if($county_id == $judet->id)
														selected
													@endif
												@endif
												data-name="{{$judet->name}}" 
												data-slug="{{$judet->slug}}" 
												value="{{$judet->id}}">{{$judet->name}} (24)</option>
											@endforeach
                                        </select>
                                    </div>
									<div class="form-group">
										<select class="form-control" id="city_id" onchange="cityChanged(this);">
											@if(isset($cities))
											    <option value="0">Toate localitatile</option>
												@foreach($cities as $city)
													<option 
													@if(isset($city_id))
														@if($city_id == $city->id)
															selected
														@endif
													@endif
													data-name="{{$city->name}}" 
													data-slug="{{$city->slug}}" 
													value="{{$city->id}}">{{$city->name}} (24)</option>
												@endforeach
											@else
												<option value="0">Selecteaza un judet</option>
											@endif
										</select>
									</div>
								</div>
								<div class="widget widget-filter">
									<div class="current-shop">
										<ul class="list-none filters">
											<li><a href="#" class="silver county"></a></li>
											<li><a href="#" class="silver city"> Turnee active</a></li>
											<li><a href="#" class="silver"> Liga 2</a></li>
											<br />
											<p class="results">56 jucatori</p>
										</ul>
										<a href="#" class="clear-all black">Reseteaza filtrele</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-shop shop-grid">
							<div class="shop-title-box">
								<h2 class="title18 title-box5">Jucatori</h2>
							</div>
							<div class="shop-banner banner-adv line-scale">
								<a href="#" class="adv-thumb-link"><img src="images/shop/banner.jpg" alt="" /></a>
							</div>
							<div class="grid-shop-product">
                                @if(count($jucatori) > 0)
                                    <div class="row">
                                        @foreach($jucatori as $jucator)
                                            <div class="col-md-4 col-sm-6 col-xs-6">
                                                <div class="item-product text-center">
													@if(1 == rand(1,3))
													<span class="product-label new-label">nou</span>
													@else 
													<span class="product-label sale-label">Liga<br/>{{$jucator->liga}}</span>
													@endif
                                                    <div class="product-thumb">
                                                        <a href="{{asset('/jucator/' . $jucator->id)}}" class="product-thumb-link zoom-thumb">
                                                            @if(!$jucator->image)
                                                            <img src="{{asset('/storage/images/genders/'.$jucator->gender.'.png')}}" alt="">
                                                            @else 
                                                              <img src="{{asset('/storage/images/players/' . $jucator->id .'/'.$jucator->image)}}" alt="">
                                                            @endif                               
                                                        </a>
                                                        <div class="product-extra-link">
                                                            <a href="{{asset('/jucator/' . $jucator->id)}}" class="addcart-link">Vezi profilul</a>
                                                        </div>
                                                    </div>
                                                    <div class="product-info">
                                                        <h3 class="product-title title14"><a href="{{asset('/jucator/' . $jucator->id)}}">{{$jucator->name}}</a></h3>
                                                        <div class="product-price">
                                                            <ins><span class="title14 color">Piatra Neamt / Neamt</span></ins>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @include('pagination.default', ['paginator' => $jucatori])
                                @else 
                                    Nu exista jucatori conform criteriilor selectate
                                @endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>



<script>
function sendFilters(){
//	console.log('trimitem filtre');
	var county_slug = $('#county_id option:selected').attr('data-slug');
	var city_slug =  $('#city_id option:selected').attr('data-slug');
	
	if(city_slug !== '0'){
		console.log('aaaaaaaa');
		window.location.href = "{{asset('/jucatori/')}}" + "/" + $('#city_id option:selected').attr('data-slug');
	} else if (county_slug !== '0'){
		console.log('bbbbbbbbbb');
		window.location.href = "{{asset('/jucatori/')}}" + "/" + $('#county_id option:selected').attr('data-slug');
	}
	
	console.log('county_slug ', county_slug);
	console.log('city_slug ', city_slug);
}
function countyChanged(judet){
	$('.filters .county').html('Judetul: ' + $('#county_id option:selected').attr('data-name'));
	$.ajax({
		type:'POST',
		url: "{{asset('/utils/getCitiesFromCounty')}}",
		data:{
			'county_id': parseInt(judet.value),
			'_token': $('meta[name="csrf-token"]').attr('content')
		},
		success:function(response){
			var htmlLocalitati = '<option data-slug="0" value="0" selected disabled>Selecteaza o localitate</option>';
			for(i=0;i<response.localitati.length; i++){
				htmlLocalitati += '<option data-slug="'+response.localitati[i].slug+'" value="'+response.localitati[i].id+'">'+response.localitati[i].name+'</option>';
			}
			$('#city_id').html(htmlLocalitati);
			sendFilters();
		}
	});
}

function cityChanged(city){
	$('.filters .city').html('Localitatea: ' + $('#city option:selected').attr('data-name'));
	sendFilters();
}


function filterLeague(button){
	console.log('liga', button);
	if($(button).hasClass('current-page')){
		console.log('e activ');
		$(button).removeClass('current-page');
	} else {
		console.log('nu e activ');
		$(button).addClass('current-page');
	}
}
</script>
@endsection