<?php

namespace App\Http\Controllers;

use DB;
use App\County;
use App\City;
use Illuminate\Http\Request;
use Auth;
use Response;


class UtilsController extends Controller 
{


    public function getCitiesFromCounty(Request $request)
    {
        $localitati = City::where('county_id', $request->county_id)->orderBy('name', 'asc')->get();
        if($localitati){
            return Response::json([
                'message' => 'Ok',
                'localitati' => $localitati
            ], 201);
        } else {
            return Response::json([
                'message' => 'Ceva nu functioneaza la server, iar provocarea nu a putut fi salvata. Va rugam sa reincercati!'
            ], 202);
        }
    }


    public static function slugify($text)
    {
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
      $text = preg_replace('~[^-\w]+~', '', $text);
      $text = trim($text, '-');
      $text = preg_replace('~-+~', '-', $text);
      $text = strtolower($text);
      if (empty($text)) {
        return 'n-a';
      }
      return $text;
    }


    public function resetOrdineAlfabeticaLocalitati()
    {
        return "functie dezactivata";
        $localitati = $old_cities = City::orderBy('name', 'asc')->get();
       // County::truncate();
        // foreach($localitati as $localitate){
        //     $localitate->delete();
        // }
        foreach($old_cities as $localitate){
            $localitate->slug = $this->slugify($localitate->name);
            $localitate->save();
        }
        return $localitati;
    }

}
