<?php

namespace App\Http\Controllers;

use App\User;
use App\Setting;
use App\Location;
use App\County;
use App\City;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function clasificare($locatie){

        
        if(null !== (County::where('slug', $locatie)->first())){
            /// Ai ales un judet pe /jucatori/{neamt} ex.
            //return $county_id;
            $county_id = County::where('slug', $locatie)->firstOrFail()->id;
            $cities = City::where('county_id', $county_id)->orderBy('name', 'asc')->get();
            $jucatori = User::where(
                [
                    'role' => 'jucator',
                    'county_id' =>  $county_id
                ]
                )->paginate(1);

            return view('jucatori/index')->with(
                [
                    "jucatori" => $jucatori,
                    'cities' => $cities,
                    'county_id' => $county_id
                ]
            );
        }

        if(null !== (City::where('slug', $locatie)->first())){
            /// Ai ales un judet pe /jucatori/{neamt} ex.
            //return $county_id;
            $city = City::where('slug', $locatie)->firstOrFail();
            $cities = City::where('county_id', $city->county_id)->orderBy('name', 'asc')->get();   
            $jucatori = User::where(
                [
                    'role' => 'jucator',
                    'city_id' =>  $city->id
                ]
                )->paginate(1);

            return view('jucatori/index')->with(
                [
                    "jucatori" => $jucatori,
                    'cities' => $cities,
                    'city_id' => $city->id,
                    'county_id' => $city->county_id
                ]
            );
        }




        
    }




    public function jucatori(){
        // for($i=8; $i<101; $i++){
        //     $jucator = new User;
        //     $jucator->id = $i;
        //     $jucator->name = "Octavian " . $i;
        //     $jucator->email = "emailgmailsasdd" . $i . ".gmail.com";
        //     $jucator->liga =  rand(1,9);
        //     $jucator->role = "jucator";
        //     $jucator->gender = rand(0,2);
        //     $jucator->activity = "new";
        //     $jucator->autodescription = "Autodescrierea mea ca jucator";
        //     $jucator->password = "random_pass";
        //     $jucator->remember_token = "asdfasdf";
        //     $jucator->save();
        // }

        // return;


        $jucatori = User::where('role','jucator')->paginate(9);
        return view('jucatori/index')->with(
            [
                "jucatori" => $jucatori
            ]
        );
    }

    public function showJucator($id){
        return view('jucatori/show')->with(
            [
                'jucator' => User::find($id)
            ]
        );
    }



    public function organizatori(){
        $organizatori = User::where('role','organizator')->paginate(2);
        return view('organizatori/index')->with(
            [
                "organizatori" => $organizatori
            ]
        );
    }

    public function showOrganizator($id){
        return view('organizatori/show')->with(
            [
                'organizator' => User::find($id),
                'locatii' => Location::where('user_id',$id)->get()
            ]
        );
    }

    public function dashboard()
    {
        return view('account/dashboard');
    }

    public function editare()
    {
        return view('account/editare');
    }

    public function permisiuni_roluri()
    {
        if(isset($_GET['rol'])){
            $rol = $_GET['rol'];
        } else {
            $rol = "nespecificat";
        }
        $slug_for_settings_table = "role_permissions_" . $rol;
        
        

        if($permisiuni_rol = Setting::find($slug_for_settings_table)){
            $permisiuni_rol = json_decode(Setting::find($slug_for_settings_table)->content);
        } else {
            $permisiuni_rol = [];
        }

        $listaPermisiuni = $this->listaPermisiuni();
        return view('permisiuni/roluri')->with(
            [
                "listaPermisiuni" => $listaPermisiuni,
                "rol" => $rol,
                "permisiuni_rol" => $permisiuni_rol
            ]
        );
    }

    public function permisiuni_utilizatori()
    {
        return view('permisiuni/utilizatori');
    }



    public function utilizatori()
    {
        $users = User::all();
        return view('account/utilizatori')->with(
            [
                "users" => $users
            ]
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('blog/show')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //
    }

    public function destroy(Article $article)
    {
        //
    }

    public function updatePermisiuni(Request $request){
        //return $request;
        if($request->update_type){
            $slug_for_settings_table = "role_permissions_" . $request->role;
            if($setting = Setting::find($slug_for_settings_table)){
                $setting = Setting::find($slug_for_settings_table);
            }else{
                $setting = new Setting;
            }
            $total = [];
            if($request->permisiuni){
                foreach($request->permisiuni as $slug_permisiune => $on){
                    $total[] = $slug_permisiune;
                }
                $setting->slug = "role_permissions_" . $request->role;
                $setting->content = json_encode($total);
                $setting->save();
            }
            return back()->with('message', 'Permisiunile pentru rolul ' . $request->role . ' au fost actualizate cu succes!');
        } else {
            return "Formular trimis incorect";
        }
        return $request;
    }

    public function listaPermisiuni(){
        $permisiuni = [
            [
                "zona" => "Articole",
                "permisiuni" => [
                    "vizualizare_articole_proprii" => "Poate vizualiza articole proprii",
                    "vizualizare_articolele_altora" => "Poate vizualiza articolele altora",
                    'creare_articole' => "Poate crea articole",
                    "editare_articole_proprii" => "Poate edita articole",
                    "editare_articolele_altora" => "Poate edita articolele altora",
                    "stergere_articole_proprii" => "Poate sterge articole proprii",
                    "stergere_articole_ale_altora" => "Poate sterge articole ale altora"
                ]
            ],
            [
                "zona" => "Turnee",
                "permisiuni" => [
                    "vizualizare_turnee_proprii" => "Poate vizualiza turneele proprii",
                    "vizualizare_turneele_altora" => "Poate vizualiza turneele altora",
                    'creare_turnee' => "Poate crea turnee",
                    "editare_turnee_proprii" => "Poate edita turnee",
                    "editare_turneele_altora" => "Poate edita turneele altora",
                    "stergere_articole_proprii" => "Poate sterge turneele proprii",
                    "stergere_articolele_altora" => "Poate sterge turneele altora"
                ]
            ],
            [
                "zona" => "Permisiuni speciale administrator",
                "permisiuni" => [
                    'adauga_utilizatori' => "Poate adauga utilizatori",
                    'edita_utilizatori' => "Poate edita utilizatori",
                    'aproba_turnee' => "Poate aproba turnee",
                    'bloca_debloca_utilizatori' => "Poate bloca/debloca utilizatori",
                ]
            ]
        ];
        return $permisiuni;
    }

}
