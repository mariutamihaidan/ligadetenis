<div class="sidebar-left sidebar-shop">
    <div class="widget widget-category-icon">
        <h2 class="title-widget title18">Filtreaza dupa judet</h2>
            <ul class="list-none list-default">
                @foreach(\App\County::all() as $judet)
                    <li><a href="{{asset('/locatii/judet/'.$judet->slug)}} ">{{$judet->name}} (16)</a></li>
                @endforeach
            </ul>
    </div>
    <!-- End Widget -->
    <div class="widget widget-filter">
        <h2 class="title-widget title18">Filtre curente</h2>
        <div class="current-shop">
            <ul class="list-none">
                <li><a href="#" class="silver"> Locatii din Neamt</a></li>
                <li><a href="#" class="silver"> Turnee active</a></li>
                <li><a href="#" class="silver"> Liga 2</a></li>
            </ul>
            <a href="#" class="clear-all black">Reseteaza filtrele</a>
        </div>
        
    </div>

</div>