@extends('layouts/frontend')
@section('content')
<section id="content">
		<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><a href="#" class="silver">Men’s </a><span class="color">Tennis</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('sidebars/organizatori')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-shop shop-grid">
							<div class="shop-title-box">
								<h2 class="title18 title-box5">Organizatori</h2>
							</div>
							<div class="shop-banner banner-adv line-scale">
								<a href="#" class="adv-thumb-link"><img src="images/shop/banner.jpg" alt="" /></a>
							</div>
							<div class="list-shop-product">
                                @if(count($organizatori) > 0)
                                    @foreach($organizatori as $organizator)
                                        <div class="item-product item-product-list">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-5 col-xs-5">
                                                    <div class="product-thumb">
                                                        <span class="product-label new-label">TOP</span>
                                                        <a href="{{asset('/organizatori/'.$organizator->id)}}" class="product-thumb-link">
															@if(!$organizator->image)
                                                          	  <img src="{{asset('/storage/images/default/organizer.png')}}" alt="">
                                                            @else 
                                                              <img src="{{asset('/storage/images/players/' . $jucator->id .'/'.$jucator->image)}}" alt="">
                                                            @endif   
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-7 col-xs-7">
                                                    <div class="product-info">
                                                        <h3 class="product-title title14">
															<a href="{{asset('/organizatori/'.$organizator->id)}}">{{$organizator->organizer_name}}</a>
														</h3>   
														<ul class="list-inline-block sku-stock">
															<li>locatii: {{count($organizator->locations)}}</li>
															<li>terenuri total: {{rand(5,30)}}</li>
														
															<li>asdfasdf</li>
														</ul>
                                                        <span class="inout-stock in-stock">
															<i class="fa fa-check-square" aria-hidden="true"></i>activ
														</span>
                                                        <p class="desc">
														{{$organizator->autodescription}}
														</p>
                                                        <div class="product-extra-link">
                                                            <a href="{{asset('/organizatori/'.$organizator->id)}}" class="addcart-link">Mai multe despre organizator</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @include('pagination.default', ['paginator' => $organizatori])
                                @else 
                                    Nu exista organizatori in sectiunea selectata
                                @endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="list-service">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form1.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Order Online</a></h3>
									<h4 class="title14 transition">Hours: 8AM -11PM</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block item-active active">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form2.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Save 30% </a></h3>
									<h4 class="title14 transition">When you use credit card</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form3.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Free Shipping</a></h3>
									<h4 class="title14 transition">On orders over $99</h4>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End List Service -->
			<div class="list-special-box">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Specials</h2>
							<div class="product-special">
								<div class="special-slider">
									<div class="wrap-item navi-bottom" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1]]">
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_1.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_2.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_3.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
									</div>
								</div>
								<a href="#" class="shop-button">View all specials</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Newsletter</h2>
							<div class="newletter-form">
								<p class="desc">Make sure you dont miss interesting hap penings by joining our newsletter program.</p>
								<form class="email-form">
									<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="your e-mail address" type="text">
									<input class="shop-button" value="Subscribe" type="submit">
								</form>
							</div>
							<h2 class="title18 font-bold">Connect with us</h2>
							<div class="social-network">
								<a href="#" class="float-shadow"><img src="images/icons/icon-fb.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-tw.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-li.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-gp.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-pt.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-sk.png" alt="" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 hidden-sm col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Video sport</h2>
							<div class="box-video">
								<a href="#" class="video-lightbox"><img src="images/home/home1/video-img.png" alt="" /></a>
								<h3 class="title14"><a href="#">Lorem ipsum dolor sit amet</a></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Special Box -->
		</div>
	</section>
	<!-- End Content -->
@endsection