@extends('layouts/frontend')
@section('content')
<div class="container">
    <div class="content-top8">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="top-adv8">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-6">
                            <div class="banner-adv item-adv1 line-scale zoom-rotate">
                                <a href="#" class="adv-thumb-link"><img src="images/home/home8/ad1.jpg" alt=""></a>
                                <div class="banner-info pos-bottom">
                                    <h2 class="title18">shoes men</h2>
                                    <a href="#" class="title14 black">shop now<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-6">
                            <div class="banner-adv item-adv1 line-scale zoom-rotate">
                                <a href="#" class="adv-thumb-link"><img src="images/home/home8/ad2.jpg" alt=""></a>
                                <div class="banner-info pos-bottom">
                                    <h2 class="title18">Shoes women</h2>
                                    <a href="#" class="title14 black">shop now<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="banner-slider banner-slick banner-slider1 hidden-pagi banner-slider8">
                    <div class="slick center">
                        <div class="item-slider">
                            <div class="banner-thumb">
                                <a href="#"><img src="images/home/home8/slide1.jpg" alt="" /></a>
                            </div>
                            <div class="banner-info">
                                <div class="banner-info1 white text-center white">
                                    <h2 class="title30">Esti in cautare de turnee?</h2>
                                    <h3 class="title14">Da click pentru a accesa lista cu cele mai apropiate turnee</h3>
                                </div>
                            </div>
                            <p class="desc-control hidden">Esti in cautare de turnee?</p>
                        </div>
                        <div class="item-slider">
                            <div class="banner-thumb">
                                <a href="#"><img src="images/home/home8/slide2.jpg" alt="" /></a>
                            </div>
                            <div class="banner-info">
                                <div class="banner-info1 white text-center white">
                                    <h2 class="title30">Esti organizator?</h2>
                                    <h3 class="title14">Doresti sa lansezi un turneu? Da click aici!</h3>
                                </div>
                            </div>
                            <p class="desc-control hidden">Esti organizator?</p>
                        </div>
                    </div>
                </div>
                <!-- End Banner Slider -->
            </div>
        </div>
    </div>
    <!-- End COntent Top -->
    <div class="content-home8">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="sidebar-home8">
                    <div class="category-icon">
                        <div class="title-box8">
                            <h2 class="title18 white">Categories</h2>
                        </div>
                        <div class="list-cat-icon">
                            <ul class="list-none">
                                <li><a href="#"><img src="images/icons/cat1.png" alt="" />Bikes store </a></li>
                                <li><a href="#"><img src="images/icons/cat2.png" alt="" />Basketball store</a></li>
                                <li><a href="#"><img src="images/icons/cat3.png" alt="" />Weightlifting</a></li>
                                <li><a href="#"><img src="images/icons/cat4.png" alt="" />Volleyball Sport</a></li>
                                <li><a href="#"><img src="images/icons/cat5.png" alt="" />Football store </a></li>
                                <li><a href="#"><img src="images/icons/cat6.png" alt="" />Golf Sport</a></li>
                                <li><a href="#"><img src="images/icons/cat7.png" alt="" />Archery store</a></li>
                                <li><a href="#"><img src="images/icons/cat8.png" alt="" />Rowing store </a></li>
                                <li><a href="#"><img src="images/icons/cat9.png" alt="" />Runner moving</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Cat Icon -->
                    <div class="poroduct-type">
                        <div class="title-box8">
                            <h2 class="title18 white">Best sellers</h2>
                        </div>
                        <div class="product-type-slider">
                            <div class="wrap-item group-navi style2" data-itemscustom="[[0,1],[560,2],[768,1]]" data-navigation="true" data-pagination="false">
                                <div class="item">
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_1.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_2.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_3.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_4.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item">
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_5.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_6.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_7.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_8.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                            </div>
                        </div>
                    </div>
                    <!-- End Product Type -->
                    <div class="poroduct-type">
                        <div class="title-box8">
                            <h2 class="title18 white">Specials</h2>
                        </div>
                        <div class="product-type-slider">
                            <div class="wrap-item group-navi style2" data-itemscustom="[[0,1],[560,2],[768,1]]" data-navigation="true" data-pagination="false">
                                <div class="item">
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_9.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_10.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_11.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_12.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item">
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_13.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_14.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_15.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product table">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_16.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                            </div>
                        </div>
                    </div>
                    <!-- End Product Type -->
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="product-box8">
                    <div class="title-box8">
                        <h2 class="title18 white inline-block">Sports Gear</h2>
                        <div class="title-tab8 inline-block">
                            <ul class="list-inline-block text-center">
                                <li><a href="#tab4" class="white" data-toggle="tab">New Arrivals </a></li>
                                <li class="active"><a href="#tab3" class="white" data-toggle="tab"> Most Reviews   </a></li>
                                <li><a href="#tab4" class="white" data-toggle="tab">Popular Products</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="tab4" class="tab-pane">
                            <div class="product-slider">
                                <div class="wrap-item group-navi style2" data-pagination="false" data-navigation="true" data-itemscustom="[[0,1],[480,2],[990,2],[1200,3]]">
                                    <div class="item-product text-center">
                                        <span class="product-label new-label">new</span>
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_1.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_13.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_6.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_11.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_3.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_2.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab3" class="tab-pane active">
                            <div class="product-slider">
                                <div class="wrap-item group-navi style2" data-pagination="false" data-navigation="true" data-itemscustom="[[0,1],[480,2],[990,2],[1200,3]]">
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_14.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_15.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_16.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_17.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_18.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_12.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Product Box -->
                <div class="list-adv8">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 hidden-xs">
                            <div class="banner-adv fade-in-out">
                                <a href="#" class="adv-thumb-link"><img src="images/home/home3/ads1.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="banner-adv fade-out-in">
                                <a href="#" class="adv-thumb-link"><img src="images/home/home3/ads2.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End List Adv -->
                <div class="product-box8">
                    <div class="title-box8">
                        <h2 class="title18 white inline-block">Sports Gear</h2>
                        <div class="title-tab8 inline-block">
                            <ul class="list-inline-block text-center">
                                <li><a href="#tab2" class="white" data-toggle="tab">For Men’s </a></li>
                                <li class="active"><a href="#tab1" class="white" data-toggle="tab">For Women’s</a></li>
                                <li><a href="#tab2" class="white" data-toggle="tab">For Kids</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="tab1" class="tab-pane active">
                            <div class="product-slider">
                                <div class="wrap-item group-navi style2" data-pagination="false" data-navigation="true" data-itemscustom="[[0,1],[480,2],[990,2],[1200,3]]">
                                    <div class="item-product text-center">
                                        <span class="product-label new-label">new</span>
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_1.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_13.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_6.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_11.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_3.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_2.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="tab-pane">
                            <div class="product-slider">
                                <div class="wrap-item group-navi style2" data-pagination="false" data-navigation="true" data-itemscustom="[[0,1],[480,2],[990,2],[1200,3]]">
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_14.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_15.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_16.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_17.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_18.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product text-center">
                                        <div class="product-thumb">
                                            <a href="detail.html" class="product-thumb-link zoom-thumb">
                                                <img src="images/photos/sport_12.jpg" alt="" />
                                            </a>
                                            <div class="product-extra-link">
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                            <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Product Box -->
                <div class="deal-box8">
                    <div class="title-box8">
                        <h2 class="title18 white">hot deals</h2>
                    </div>
                    <div class="product-slider">
                        <div class="wrap-item group-navi style2" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1],[560,2],[990,3]]">
                            <div class="item-product text-center">
                                <span class="product-label sale-label">50% off</span>
                                <div class="product-thumb">
                                    <a href="detail.html" class="product-thumb-link zoom-thumb">
                                        <img src="images/photos/sport_12.jpg" alt="">
                                    </a>
                                    <div class="product-extra-link">
                                        <a href="#" class="wishlist-link"></a>
                                        <a href="#" class="addcart-link">Add to cart</a>
                                        <a href="#" class="compare-link"></a>
                                    </div>
                                    <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                </div>
                                <div class="product-info">
                                    <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                    <div class="product-price">
                                        <del><span class="title14 silver">$798.00</span></del>
                                        <ins><span class="title14 color">$399.00</span></ins>
                                    </div>
                                    <div class="countdown-master"></div>
                                </div>
                            </div>
                            <div class="item-product text-center">
                                <span class="product-label sale-label">50% off</span>
                                <div class="product-thumb">
                                    <a href="detail.html" class="product-thumb-link zoom-thumb">
                                        <img src="images/photos/sport_2.jpg" alt="">
                                    </a>
                                    <div class="product-extra-link">
                                        <a href="#" class="wishlist-link"></a>
                                        <a href="#" class="addcart-link">Add to cart</a>
                                        <a href="#" class="compare-link"></a>
                                    </div>
                                    <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                </div>
                                <div class="product-info">
                                    <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                    <div class="product-price">
                                        <del><span class="title14 silver">$798.00</span></del>
                                        <ins><span class="title14 color">$399.00</span></ins>
                                    </div>
                                    <div class="countdown-master"></div>
                                </div>
                            </div>
                            <div class="item-product text-center">
                                <span class="product-label sale-label">50% off</span>
                                <div class="product-thumb">
                                    <a href="detail.html" class="product-thumb-link zoom-thumb">
                                        <img src="images/photos/sport_18.jpg" alt="">
                                    </a>
                                    <div class="product-extra-link">
                                        <a href="#" class="wishlist-link"></a>
                                        <a href="#" class="addcart-link">Add to cart</a>
                                        <a href="#" class="compare-link"></a>
                                    </div>
                                    <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                </div>
                                <div class="product-info">
                                    <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                    <div class="product-price">
                                        <del><span class="title14 silver">$798.00</span></del>
                                        <ins><span class="title14 color">$399.00</span></ins>
                                    </div>
                                    <div class="countdown-master"></div>
                                </div>
                            </div>
                            <div class="item-product text-center">
                                <span class="product-label sale-label">50% off</span>
                                <div class="product-thumb">
                                    <a href="detail.html" class="product-thumb-link zoom-thumb">
                                        <img src="images/photos/sport_5.jpg" alt="">
                                    </a>
                                    <div class="product-extra-link">
                                        <a href="#" class="wishlist-link"></a>
                                        <a href="#" class="addcart-link">Add to cart</a>
                                        <a href="#" class="compare-link"></a>
                                    </div>
                                    <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                </div>
                                <div class="product-info">
                                    <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                    <div class="product-price">
                                        <del><span class="title14 silver">$798.00</span></del>
                                        <ins><span class="title14 color">$399.00</span></ins>
                                    </div>
                                    <div class="countdown-master"></div>
                                </div>
                            </div>
                            <div class="item-product text-center">
                                <span class="product-label sale-label">50% off</span>
                                <div class="product-thumb">
                                    <a href="detail.html" class="product-thumb-link zoom-thumb">
                                        <img src="images/photos/sport_6.jpg" alt="">
                                    </a>
                                    <div class="product-extra-link">
                                        <a href="#" class="wishlist-link"></a>
                                        <a href="#" class="addcart-link">Add to cart</a>
                                        <a href="#" class="compare-link"></a>
                                    </div>
                                    <a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
                                </div>
                                <div class="product-info">
                                    <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                    <div class="product-price">
                                        <del><span class="title14 silver">$798.00</span></del>
                                        <ins><span class="title14 color">$399.00</span></ins>
                                    </div>
                                    <div class="countdown-master"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Deal Box -->
            </div>
        </div>
    </div>
    <!-- End Content Home -->
    <div class="hot-cat8">
        <h2 class="title18">hot categories</h2>
        <div class="hotcat-slider3">
            <div class="wrap-item group-navi style2" data-itemscustom="[[0,1],[480,2],[768,3],[990,4]]" data-pagination="false" data-navigation="true">
                <div class="cat-item3 border text-center">
                    <div class="banner-adv zoom-out">
                        <a href="#" class="adv-thumb-link">
                            <img src="images/home/home3/cat1.jpg" alt="" />
                            <img src="images/home/home3/cat1.jpg" alt="" />
                        </a>
                    </div>
                    <div class="cat-title">
                        <a href="#"><span class="title14">accessories</span><span class="silver">(20)</span></a>
                    </div>
                </div>
                <div class="cat-item3 border text-center">
                    <div class="banner-adv zoom-out">
                        <a href="#" class="adv-thumb-link">
                            <img src="images/home/home3/cat2.jpg" alt="" />
                            <img src="images/home/home3/cat2.jpg" alt="" />
                        </a>
                    </div>
                    <div class="cat-title">
                        <a href="#"><span class="title14">shoes</span><span class="silver">(15)</span></a>
                    </div>
                </div>
                <div class="cat-item3 border text-center">
                    <div class="banner-adv zoom-out">
                        <a href="#" class="adv-thumb-link">
                            <img src="images/home/home3/cat3.jpg" alt="" />
                            <img src="images/home/home3/cat3.jpg" alt="" />
                        </a>
                    </div>
                    <div class="cat-title">
                        <a href="#"><span class="title14">Clothing</span><span class="silver">(60)</span></a>
                    </div>
                </div>
                <div class="cat-item3 border text-center">
                    <div class="banner-adv zoom-out">
                        <a href="#" class="adv-thumb-link">
                            <img src="images/home/home3/cat4.jpg" alt="" />
                            <img src="images/home/home3/cat4.jpg" alt="" />
                        </a>
                    </div>
                    <div class="cat-title">
                        <a href="#"><span class="title14">tennis</span><span class="silver">(32)</span></a>
                    </div>
                </div>
                <div class="cat-item3 border text-center">
                    <div class="banner-adv zoom-out">
                        <a href="#" class="adv-thumb-link">
                            <img src="images/home/home3/cat5.jpg" alt="" />
                            <img src="images/home/home3/cat5.jpg" alt="" />
                        </a>
                    </div>
                    <div class="cat-title">
                        <a href="#"><span class="title14">Dress</span><span class="silver">(10)</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Hot Cat -->
</div>
@endsection