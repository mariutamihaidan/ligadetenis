@extends('layouts/frontend')
@section('content')
<section id="content">
		<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><a href="#" class="silver">Men’s </a><span class="color">Tennis</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('sidebars/organizatori')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="content-blog">
                            <h2 class="title18 title-box5">Ultimele meciuri</h2>
                            @if(count($meciuri) > 0)
                                    <table class="table table-dark">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                            <th scope="col">Rezultat</th>
                                            <th scope="col">Din</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($meciuri as $meci)
                                                @if($meci->type == 1)
                                                    <tr>
                                                        <th scope="row">{{$meci->id}}</th>
                                                        <td>
                                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                                        </td>
                                                        @if($meci->winner == 1)
                                                            <td style="color: green; font-weight:bold">{{$meci->player1->name}}</td>
                                                        @else
                                                            <td>{{$meci->player1->name}}</td>
                                                        @endif
                                                        @if($meci->winner == 2)
                                                            <td style="color: green; font-weight:bold">{{$meci->player3->name}}</td>
                                                        @else
                                                            <td>{{$meci->player3->name}}</td>
                                                        @endif
                                                        <td>{{$meci->result}}</td>
                                                        <td>{{$meci->created_at}}</td>
                                                    </tr>
                                                @else
                                                <tr>
                                                        <th scope="row">{{$meci->id}}</th>
                                                        <td>
                                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                                        </td>
                                                        @if($meci->winner == 1)
                                                            <td style="color: green; font-weight:bold">{{$meci->player1->name}} - {{$meci->player2->name}}</td>
                                                        @else
                                                            <td>{{$meci->player1->name}} - {{$meci->player2->name}}</td>
                                                        @endif
                                                        @if($meci->winner == 2)
                                                            <td style="color: green; font-weight:bold">{{$meci->player3->name}} - {{$meci->player4->name}}</td>
                                                        @else
                                                            <td>{{$meci->player3->name}} - {{$meci->player4->name}}</td>
                                                        @endif
                                                        <td>{{$meci->result}}</td>
                                                        <td>{{$meci->created_at}}</td>
                                                    </tr>
                                                @endif
                                            
                                                
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @include('pagination.default', ['paginator' => $meciuri])
                                @else 
                                    Nu exista meciuri conform criteriilor
                                @endif
                        </div>
					</div>					
				</div>
			</div>
		</div>
	</section>
@endsection