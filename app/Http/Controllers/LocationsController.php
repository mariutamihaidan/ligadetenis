<?php

namespace App\Http\Controllers;

use DB;
use App\Location;
use Illuminate\Http\Request;
use Auth;
use Response;


class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $articles = Article::paginate(1);
         $latestArticles = Article::orderBy('created_at', 'desc')
                         ->take(10)
                         ->get();
  
         return view('blog/index')->with(
             [
                 "latestArticles" => $latestArticles,
                 "articles" => $articles ,
                 'categories' => Category::all()
             ]
         );
    }

   


    public function create()
    {
        return view('blog/create')->with(
            [
                'allCategories' => Category::all()
            ]
        );
    }


    public function store(Request $request)
    {
        if($this->challangeExists(Auth::user()->id, $request->receiver_id)){
            return Response::json([
                'message' => 'Aceasta provocare deja exista.. Asteptati mesajul de la persoana provocata!'
            ], 202);
        }

        $challange = new Challange;
        $challange->sender_id = Auth::user()->id;
        $challange->receiver_id = $request->receiver_id;
        $challange->location_id = $request->location_id;
        $challange->timestamp = strtotime($request->timestamp);
        $challange->message = $request->message;
        $challange->status = 0;
        
        if($challange->save()){
            return Response::json([
                'message' => 'Provocarea a fost trimisa! Trebuie sa asteptati raspunsul de la acest jucator!'
            ], 201);
        } else {
            return Response::json([
                'message' => 'Ceva nu functioneaza la server, iar provocarea nu a putut fi salvata. Va rugam sa reincercati!'
            ], 202);
        }
        return $challange;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('locations/show')->with(
            [ 
                'location' => Location::find($id)
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('blog/edit')->with(
            [
                'article' => $article,
                'allCategories' => Category::all(),
                'categories' => ArticlesInCategories::where('article_id',$article->id)->pluck('category_id')->toArray()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
       // return $request->id;
        $article = Article::find($request->id);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->content = $request->content;
        $article->save();

        // STergem toate categoriile din acest articol ca sa reintroducem datele noi in categorii
        $articles_in_categories = ArticlesInCategories::where('article_id',$article->id);
        $articles_in_categories->delete();
      //  return $request->categories;
        if(isset($request->categories)){
            foreach($request->categories as $category){
                $article_in_category = new ArticlesInCategories;
                $article_in_category->category_id = $category;
                $article_in_category->article_id = $request->id;
                $article_in_category->save();
            }
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
