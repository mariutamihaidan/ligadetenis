@extends('layouts/frontend')
@section('content')
	<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">Permisiuni roluri</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						@include('/inc/dashboard_menu')
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-blog">
                            <form class="block-login"  method="POST" action="{{ asset('/permisuni/roluri') }}">
                                @csrf
                                <input type="hidden" name="update_type" value="role" />     
                                @if (session('message'))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif
                                <h2 class="title18 title-box5">Permisiuni roluri 
                                    <div style="float:right" class="permissions_select_role">
                                        <div class="form-group">
                                            <select name="role" class="form-control" onchange="rolSchimbat()" id="role">
                                                <option {{ $rol == "nespecificat" ? "selected" : "" }} value="nespecificat" disabled>Alege un rol</option>     
                                                <option {{ $rol == "jucator" ? "selected" : "" }} value="jucator">Jucator</option>     
                                                <option {{ $rol == "organizator" ? "selected" : "" }} value="organizator">Organizator</option>     
                                                <option {{ $rol == "administrator" ? "selected" : "" }} value="administrator">Administrator</option>   
                                            </select>
                                            <button class="btn btn-success">Salveaza</button>
                                        </div>
                                    </div>
                                </h2>
                                @if(!isset($_GET['rol']))    
                                    Niciun rol selectat..
                                @else 
                                    <div class="row">
                                        @foreach($listaPermisiuni as $element)
                                            <div class="col-md-4">
                                                <span style="text-transform:uppercase;"> {{$element['zona']}}</span> <br />
                                                @foreach($element['permisiuni'] as $slug => $permisiune )
                                                    <label style="font-weight:100; cursor:pointer;">
                                                        <input name="permisiuni[{{$slug}}]" type="checkbox" {{ in_array($slug,$permisiuni_rol) ? "checked": ""}}/> {{$permisiune}}
                                                    </label> <br />
                                                @endforeach
                                                <hr  />
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
				</div>
			</div>
			<!-- End Special Box -->
        </div>
<script>
function rolSchimbat(){
    console.log($('#role').val());
    var role_slug = $('#role').val();
    window.location.href = "{{asset('/permisiuni/roluri?rol=')}}" + role_slug;
}

</script>
@endsection