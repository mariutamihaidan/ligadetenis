<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/home');
});
Route::get('/despre', function () { return view('pages/despre'); });
Route::get('/regulament', function () { return view('pages/regulament'); });
Route::get('/contact', function () { return view('pages/contact'); });


Route::get('/articole/categorie/{slug}', 'ArticleController@categorie');
Route::get('/articole/editAll', 'ArticleController@editAll');
Route::get('/locatii/{id}', 'LocationsController@show');
Route::resource('/articole', 'ArticleController');

Route::resource('/categorii', 'CategoriesController');
Route::resource('/meciuri', 'GamesController');
Route::get('/provocari/trimise', 'ChallangesController@sent');
Route::get('/provocari/primite', 'ChallangesController@received');
Route::resource('/provocari', 'ChallangesController');



// Account Group
Route::get('/cont', 'AccountController@dashboard');


Route::get('/jucatori', 'AccountController@jucatori');
Route::get('/jucator/{id}', 'AccountController@showJucator');    // aici scrie jucator la SINGULAR
Route::get('/jucatori/{locatie}', 'AccountController@clasificare');


Route::get('/organizatori', 'AccountController@organizatori');
Route::get('/organizatori/{id}', 'AccountController@showOrganizator');
Route::get('/cont/editare', 'AccountController@editare');



Route::get('/permisiuni/roluri', 'AccountController@permisiuni_roluri');
Route::post('/permisuni/roluri', 'AccountController@updatePermisiuni');

Route::get('/permisiuni/utilizatori', 'AccountController@permisiuni_utilizatori');
Route::get('/permisiuni/listaPermisiuni', 'AccountController@listaPermisiuni');



Route::get('/utilizatori', 'AccountController@utilizatori');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 

// Utils
Route::post('/utils/getCitiesFromCounty', 'UtilsController@getCitiesFromCounty');
//Route::get('/utils/resetOrdineAlfabeticaLocalitati', 'UtilsController@resetOrdineAlfabeticaLocalitati');